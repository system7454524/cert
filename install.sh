#!/bin/sh

DISTRUBUTION=$(( lsb_release -ds || cat /etc/*release || uname -om ) 2>/dev/null | head -n1 | cut -d ' ' -f 1)

DOSSIER="/etc/ssl/certs/"

case $DISTRUBUTION in
    "NAME=\"Red" )
        DOSSIER="/etc/pki/ca-trust/source/anchors/"
    ;;
    "AlmaLinux" )
        DOSSIER="/etc/pki/ca-trust/source/anchors/"
    ;;
    "Debian" )
        DOSSIER="/usr/local/share/ca-certificates/"
    ;;
esac

CERTS=CA-ROOT.crt,CA-CHILD.crt,CA-CHILD-STEP.crt

for CERT in ${CERTS//,/ }
do
    curl $(echo "https://gitlab.com/system7454524/cert/-/raw/main/${CERT}") --output $(echo "${DOSSIER}${CERT}") -s
done


case $DISTRUBUTION in
    "NAME=\"Red" )
        update-ca-trust
    ;;
    "AlmaLinux" )
        update-ca-trust
    ;;
    "Debian" )
        update-ca-certificates
    ;;
esac

